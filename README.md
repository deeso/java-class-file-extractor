# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository holds the source code for a utility that can be used to extract Java class files from memory.  The source code is based on structures and code found in the Java class file analysis implementation.   This utility is based on, in large part, code I contributed to radare for handling Java Class files.  This code also contains some structures that were defined in the core of the radare project.

### How do I get set up? ###

* Compilation instructions are in the source files.