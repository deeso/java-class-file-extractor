/* radare - Apache 2.0 - Copyright 2007-2014 - pancake
   class.h rewrite: Adam Pridgen <dso@rice.edu || adam.pridgen@thecoverofnight.com>
 */

#ifndef _INCLUDE_JAVA_CLASS_H_
#define _INCLUDE_JAVA_CLASS_H_

#include "dsojson.h"
#include <stdint.h>
#include <stdio.h>

#define USHORT(x,y) ((uint16_t)(x[y+1]|(x[y]<<8)))
#define UINT(x,y) ((uint32_t)((x[y]<<24)|(x[y+1]<<16)|(x[y+2]<<8)|x[y+3]))

#define R_BIN_JAVA_MAXSTR 256

#define R_BIN_JAVA_USHORT(x,y) ((uint16_t)(((0xff&x[y+1])|((x[y]&0xff)<<8)) & 0xffff))

#define R_BIN_JAVA_UINT(x,y) ((uint32_t)(((x[y]&0xff)<<24)|((x[y+1]&0xff)<<16)|((x[y+2]&0xff)<<8)|(x[y+3]&0xff)))
#define R_BIN_JAVA_FLOAT(x,y) ((float)R_BIN_JAVA_UINT(x,y))

#define R_BIN_JAVA_LONG(x,y) ( ((uint64_t) R_BIN_JAVA_UINT (x, y) << 32) | ((uint64_t)R_BIN_JAVA_UINT (x, y+4) & 0xffffffff))
//#define R_BIN_JAVA_DOUBLE(x,y) ((double)RBIN_JAVA_LONG(x,y))
//#define R_BIN_JAVA_SWAPUSHORT(x) ((uint16_t)((x<<8)|((x>>8)&0x00FF)))

#define eprintf(x,y...) fprintf(stderr,x,##y)

#define uint64_t_MAX (uint64_t) -1
#define uint32_t_MAX (uint32_t) -1
#define BOOL_FALSE 0
#define BOOL_TRUE 1

#define NEW0(x) (x*)calloc(1,sizeof(x))

#define R_BIN_JAVA_DOUBLE(x,y) rbin_java_raw_to_double(x, y)


// Start borrowed code
#define R_BIN_SIZEOF_STRINGS 4096

typedef void (*RListFree)(void *ptr);
typedef struct r_list_iter_t {
	void *data;
	struct r_list_iter_t *n, *p;
} RListIter;

typedef struct r_list_t {
	RListIter *head;
	RListIter *tail;
	RListFree free;
} RList;


enum {
	R_BIN_SYM_ENTRY,
	R_BIN_SYM_INIT,
	R_BIN_SYM_MAIN,
	R_BIN_SYM_FINI,
	R_BIN_SYM_LAST
};

typedef struct r_bin_string_t {
	// TODO: rename string->name (avoid colisions)
	char string[R_BIN_SIZEOF_STRINGS+1];
	uint64_t vaddr;
	uint64_t paddr;
	uint32_t ordinal;
	uint32_t size; // size of buffer containing the string in bytes
	uint32_t length; // length of string in chars
	char type; // Ascii Wide cp850 utf8 ...
} RBinString;

typedef struct r_bin_field_t {
	char name[R_BIN_SIZEOF_STRINGS+1];
	uint64_t vaddr;
	uint64_t paddr;
	uint32_t visibility;
} RBinField;


typedef struct r_bin_symbol_t {
	char name[R_BIN_SIZEOF_STRINGS+1];
	char forwarder[R_BIN_SIZEOF_STRINGS+1];
	char bind[R_BIN_SIZEOF_STRINGS+1];
	char type[R_BIN_SIZEOF_STRINGS+1];
	char visibility_str[R_BIN_SIZEOF_STRINGS+1];
	char classname[R_BIN_SIZEOF_STRINGS+1];
	char descriptor[R_BIN_SIZEOF_STRINGS+1];
	uint64_t vaddr;
	uint64_t paddr;
	uint32_t size;
	uint32_t ordinal;
	uint32_t visibility;
	// int bits;
} RBinSymbol;

typedef struct r_bin_addr_t {
	uint64_t vaddr;
	uint64_t paddr;
} RBinAddr;

typedef struct r_bin_class_t {
	char *name;
	char *super;
	char *visibility_str;
	int index;
	RList *methods; // <RBinSymbol>
	RList *fields; // <RBinField>
	int visibility;
} RBinClass;

typedef struct r_bin_import_t {
	char name[R_BIN_SIZEOF_STRINGS+1];
	char bind[R_BIN_SIZEOF_STRINGS+1];
	char type[R_BIN_SIZEOF_STRINGS+1];
	char classname[R_BIN_SIZEOF_STRINGS+1];
	char descriptor[R_BIN_SIZEOF_STRINGS+1];
	uint32_t ordinal;
	uint32_t visibility;
} RBinImport;

typedef struct r_bin_section_t {
	char name[R_BIN_SIZEOF_STRINGS+1];
	uint64_t size;
	uint64_t vsize;
	uint64_t vaddr;
	uint64_t paddr;
	uint64_t srwx;
	// per section platform info
	const char *arch;
	int bits;
} RBinSection;


#define r_list_foreach(list, it, pos) \
	if (list) for (it = list->head; it && (pos = it->data); it = it->n)
/* Safe when calling r_list_delete() while iterating over the list. */
#define r_list_foreach_safe(list, it, tmp, pos) \
	if (list) for (it = list->head; it && (pos = it->data) && ((tmp = it->n) || 1); it = tmp)
#define r_list_foreach_prev(list, it, pos) \
	if (list) for (it = list->tail; it && (pos = it->data); it = it->p)

void r_list_init(RList *);
RList *r_list_new();
RList *r_list_newf(RListFree f);
void *r_list_get_n(RList *list, int n);
RListIter *r_list_append(RList *list, void *data);
int r_list_length(RList *list);
void r_list_free (RList *list);
RListIter *r_list_iterator (RList *list);
void r_list_delete (RList *list, RListIter *iter);
void r_list_split_iter (RList *list, RListIter *iter);
char *r_str_dup(char *ptr, const char *string);
void r_list_purge (RList *list);
// End borrowed code


typedef enum {
	R_BIN_JAVA_METHOD_ACC_PUBLIC= 0x0001,
	R_BIN_JAVA_METHOD_ACC_PRIVATE = 0x0002,
	R_BIN_JAVA_METHOD_ACC_PROTECTED = 0x0004,
	R_BIN_JAVA_METHOD_ACC_STATIC = 0x0008,

	R_BIN_JAVA_METHOD_ACC_FINAL = 0x0010,
	R_BIN_JAVA_METHOD_ACC_SYNCHRONIZED = 0x0020,
	R_BIN_JAVA_METHOD_ACC_BRIDGE = 0x0040,
	R_BIN_JAVA_METHOD_ACC_VARARGS = 0x0080,

	R_BIN_JAVA_METHOD_ACC_NATIVE = 0x0100,
	R_BIN_JAVA_METHOD_ACC_INTERFACE = 0x0200,
	R_BIN_JAVA_METHOD_ACC_ABSTRACT = 0x0400,
	R_BIN_JAVA_METHOD_ACC_STRICT= 0x0800,

	R_BIN_JAVA_METHOD_ACC_SYNTHETIC = 0x1000,
	R_BIN_JAVA_METHOD_ACC_ANNOTATION = 0x2000,
	R_BIN_JAVA_METHOD_ACC_ENUM = 0x4000
} R_BIN_JAVA_METHOD_ACCESS;

typedef enum {
	R_BIN_JAVA_FIELD_ACC_PUBLIC= 0x0001,
	R_BIN_JAVA_FIELD_ACC_PRIVATE = 0x0002,
	R_BIN_JAVA_FIELD_ACC_PROTECTED = 0x0004,
	R_BIN_JAVA_FIELD_ACC_STATIC = 0x0008,

	R_BIN_JAVA_FIELD_ACC_FINAL = 0x0010,
	R_BIN_JAVA_FIELD_ACC_VOLATILE = 0x0040,
	R_BIN_JAVA_FIELD_ACC_TRANSIENT = 0x0080,

	R_BIN_JAVA_FIELD_ACC_SYNTHETIC = 0x1000,
	R_BIN_JAVA_FIELD_ACC_ENUM = 0x4000
} R_BIN_JAVA_FIELD_ACCESS;

typedef enum {
	R_BIN_JAVA_CLASS_ACC_PUBLIC= 0x0001,
	R_BIN_JAVA_CLASS_ACC_PRIVATE = 0x0002,
	R_BIN_JAVA_CLASS_ACC_PROTECTED = 0x0004,
	R_BIN_JAVA_CLASS_ACC_STATIC = 0x0008,

	R_BIN_JAVA_CLASS_ACC_FINAL = 0x0010,
	R_BIN_JAVA_CLASS_ACC_SUPER = 0x0020,
	R_BIN_JAVA_CLASS_ACC_BRIDGE = 0x0040,
	R_BIN_JAVA_CLASS_ACC_VARARGS = 0x0080,

	R_BIN_JAVA_CLASS_ACC_NATIVE = 0x0100,
	R_BIN_JAVA_CLASS_ACC_INTERFACE = 0x0200,
	R_BIN_JAVA_CLASS_ACC_ABSTRACT = 0x0400,
	R_BIN_JAVA_CLASS_ACC_STRICT= 0x0800,

	R_BIN_JAVA_CLASS_ACC_SYNTHETIC = 0x1000,
	R_BIN_JAVA_CLASS_ACC_ANNOTATION = 0x2000,
	R_BIN_JAVA_CLASS_ACC_ENUM = 0x4000
} R_BIN_JAVA_CLASS_ACCESS;

typedef struct {
	char *str;
	uint16_t value;
	uint8_t len;
} RBinJavaAccessFlags;

typedef enum {
	R_BIN_JAVA_REF_UNKNOWN = 0,
	R_BIN_JAVA_REF_GETFIELD = 1,
	R_BIN_JAVA_REF_GETSTATIC = 2,
	R_BIN_JAVA_REF_PUTFIELD = 3,
	R_BIN_JAVA_REF_PUTSTATIC = 4,
	R_BIN_JAVA_REF_INVOKEVIRTUAL = 5,
	R_BIN_JAVA_REF_INVOKESTATIC = 6,
	R_BIN_JAVA_REF_INVOKESPECIAL = 7,
	R_BIN_JAVA_REF_NEWINVOKESPECIAL = 8,
	R_BIN_JAVA_REF_INVOKEINTERFACE = 9
} R_BIN_JAVA_REF_TYPE;


typedef enum {
	R_BIN_JAVA_CP_NULL = 0,
	R_BIN_JAVA_CP_UTF8 = 1,
	R_BIN_JAVA_CP_UNKNOWN = 2,
	R_BIN_JAVA_CP_INTEGER = 3,
	R_BIN_JAVA_CP_FLOAT = 4,
	R_BIN_JAVA_CP_LONG = 5,
	R_BIN_JAVA_CP_DOUBLE = 6,
	R_BIN_JAVA_CP_CLASS = 7,
	R_BIN_JAVA_CP_STRING = 8,
	R_BIN_JAVA_CP_FIELDREF = 9,
	R_BIN_JAVA_CP_METHODREF = 10,
	R_BIN_JAVA_CP_INTERFACEMETHOD_REF = 11,
	R_BIN_JAVA_CP_NAMEANDTYPE = 12,
	R_BIN_JAVA_CP_NOTHIN_13 = 13,
	R_BIN_JAVA_CP_NOTHIN_14 = 14,
	R_BIN_JAVA_CP_METHODHANDLE = 15,
	R_BIN_JAVA_CP_METHODTYPE = 16,
	R_BIN_JAVA_CP_NOTHIN_17 = 17,
	R_BIN_JAVA_CP_INVOKEDYNAMIC = 18,
} R_BIN_JAVA_CP_TYPE;

typedef enum {
	R_BIN_JAVA_STACK_FRAME_IMPLICIT = 0,
	R_BIN_JAVA_STACK_FRAME_SAME,
	R_BIN_JAVA_STACK_FRAME_SAME_LOCALS_1,
	R_BIN_JAVA_STACK_FRAME_CHOP,
	R_BIN_JAVA_STACK_FRAME_SAME_FRAME_EXTENDED,
	R_BIN_JAVA_STACK_FRAME_APPEND,
	R_BIN_JAVA_STACK_FRAME_FULL_FRAME,
	R_BIN_JAVA_STACK_FRAME_RESERVED
} R_BIN_JAVA_STACK_FRAME_TYPE;


typedef enum {
	R_BIN_JAVA_ATTR_TYPE_ANNOTATION_DEFAULT_ATTR = 0,
	R_BIN_JAVA_ATTR_TYPE_BOOTSTRAP_METHODS_ATTR,
	R_BIN_JAVA_ATTR_TYPE_CODE_ATTR,
	R_BIN_JAVA_ATTR_TYPE_CONST_VALUE_ATTR,
	R_BIN_JAVA_ATTR_TYPE_DEPRECATED_ATTR,
	R_BIN_JAVA_ATTR_TYPE_ENCLOSING_METHOD_ATTR,
	R_BIN_JAVA_ATTR_TYPE_EXCEPTIONS_ATTR,
	R_BIN_JAVA_ATTR_TYPE_INNER_CLASSES_ATTR,
	R_BIN_JAVA_ATTR_TYPE_LINE_NUMBER_TABLE_ATTR,
	R_BIN_JAVA_ATTR_TYPE_LOCAL_VARIABLE_TABLE_ATTR,
	R_BIN_JAVA_ATTR_TYPE_LOCAL_VARIABLE_TYPE_TABLE_ATTR,
	R_BIN_JAVA_ATTR_TYPE_RUNTIME_INVISIBLE_ANNOTATION_ATTR,
	R_BIN_JAVA_ATTR_TYPE_RUNTIME_INVISIBLE_PARAMETER_ANNOTATION_ATTR,
	R_BIN_JAVA_ATTR_TYPE_RUNTIME_VISIBLE_ANNOTATION_ATTR,
	R_BIN_JAVA_ATTR_TYPE_RUNTIME_VISIBLE_PARAMETER_ANNOTATION_ATTR,
	R_BIN_JAVA_ATTR_TYPE_SIGNATURE_ATTR,
	R_BIN_JAVA_ATTR_TYPE_SOURCE_DEBUG_EXTENTSION_ATTR,
	R_BIN_JAVA_ATTR_TYPE_SOURCE_FILE_ATTR,
	R_BIN_JAVA_ATTR_TYPE_STACK_MAP_TABLE_ATTR,
	R_BIN_JAVA_ATTR_TYPE_SYNTHETIC_ATTR,
	R_BIN_JAVA_ATTR_TYPE_UNKNOWN_ATTR,
	R_BIN_JAVA_ATTR_TYPE_FAILED_ATTR
} R_BIN_JAVA_ATTR_TYPE;

typedef enum {
	R_BIN_JAVA_STACKMAP_TOP = 0,
	R_BIN_JAVA_STACKMAP_INTEGER,
	R_BIN_JAVA_STACKMAP_FLOAT,
	R_BIN_JAVA_STACKMAP_DOUBLE,
	R_BIN_JAVA_STACKMAP_LONG,
	R_BIN_JAVA_STACKMAP_NULL,
	R_BIN_JAVA_STACKMAP_THIS,
	R_BIN_JAVA_STACKMAP_OBJECT,
	R_BIN_JAVA_STACKMAP_UNINIT,
	R_BIN_JAVA_STACKMAP_UNKNOWN
} R_BIN_JAVA_STACKMAP_TYPE;

typedef enum {
	R_BIN_JAVA_FIELD_TYPE_FIELD = 0,
	R_BIN_JAVA_FIELD_TYPE_METHOD,
	R_BIN_JAVA_FIELD_TYPE_INTERFACE
} R_BIN_JAVA_FIELD_TYPE;

typedef struct r_bin_java_meta{
	uint64_t  file_offset;
	void *type_info;
	uint32_t ord;
}  RBinJavaMetaInfo;

/* Class Reference Informations */

//struct r_bin_R_BIN_JAVA_CP_ant_t;

typedef struct  r_bin_java_class_info_t {
	uint16_t name_idx;
} RBinJavaCPTypeClass;

typedef struct  r_bin_java_fieldref_info_t {
	uint16_t class_idx;
	uint16_t name_and_type_idx;
} RBinJavaCPTypeFieldRef;

typedef struct  r_bin_java_methodref_info_t {
	uint16_t class_idx;
	uint16_t name_and_type_idx;
} RBinJavaCPTypeMethodRef;


typedef struct  r_bin_java_interfacemethodref_info_t {
	uint16_t class_idx;
	uint16_t name_and_type_idx;
} RBinJavaCPTypeInterfaceMethodRef;

typedef struct  r_bin_java_methodhandle_info_t {
	uint8_t reference_kind;
	uint16_t reference_index;
} RBinJavaCPTypeMethodHandle;

typedef struct  r_bin_java_methodtype_info_t {
	uint16_t descriptor_index;
} RBinJavaCPTypeMethodType;

typedef struct  r_bin_java_invokedynamic_info_t {
	uint16_t bootstrap_method_attr_index;
	uint16_t name_and_type_index;
} RBinJavaCPTypeInvokeDynamic;

/* Primitive Type Informations */

typedef struct  r_bin_java_string_info_t {
	uint16_t string_idx;
} RBinJavaCPTypeString;

typedef struct  r_bin_java_integer_info_t {
	union {
		uint8_t raw[4];
		uint32_t dword;
	} bytes;

} RBinJavaCPTypeInteger;

typedef struct  r_bin_java_float_info_t {
	union {
		uint8_t raw[4];
		uint32_t dword;
	} bytes;
} RBinJavaCPTypeFloat;


typedef struct  r_bin_java_long_info_t {
	union {
		uint8_t raw[8];
		uint64_t qword;
		struct{
			uint32_t high;
			uint32_t low;
		} dwords;
	} bytes;
} RBinJavaCPTypeLong;

typedef struct  r_bin_java_double_info_t {
	union {
		uint8_t raw[8];
		struct{
			uint32_t high;
			uint32_t low;
		} dwords;
	} bytes;
} RBinJavaCPTypeDouble;


/* Meta-data Info */

typedef struct  r_bin_java_name_and_type_info_t {
	uint16_t name_idx;
	uint16_t descriptor_idx;
} RBinJavaCPTypeNameAndType;

typedef struct  r_bin_java_utf8_info_t {
	uint16_t length;
	uint8_t *bytes;
} RBinJavaCPTypeUtf8;

typedef struct  r_bin_java_cp_object_t {
	RBinJavaMetaInfo *metas;
	uint64_t file_offset;
	uint8_t tag;
	union {
		RBinJavaCPTypeClass cp_class;
		RBinJavaCPTypeMethodRef cp_method;
		RBinJavaCPTypeFieldRef cp_field;
		RBinJavaCPTypeInterfaceMethodRef cp_interface;

		RBinJavaCPTypeString cp_string;
		RBinJavaCPTypeInteger cp_integer;
		RBinJavaCPTypeFloat cp_float;
		RBinJavaCPTypeLong cp_long;
		RBinJavaCPTypeDouble cp_double;
		RBinJavaCPTypeNameAndType cp_name_and_type;
		RBinJavaCPTypeUtf8 cp_utf8;

		RBinJavaCPTypeMethodHandle cp_method_handle;
		RBinJavaCPTypeMethodType cp_method_type;
		RBinJavaCPTypeInvokeDynamic cp_invoke_dynamic;
	} info;
	char* name;
	uint8_t* value;
	uint64_t loadaddr;
	uint16_t idx;
} RBinJavaCPTypeObj;

typedef struct r_bin_java_stack_map_frame_t { // attribute StackMap
	uint64_t file_offset;
	uint64_t size;
	uint32_t code_offset;
	uint8_t tag;
	uint8_t type;

	struct r_bin_java_stack_map_frame_t *p_stack_frame;

	RBinJavaMetaInfo *metas;
	uint32_t offset_delta;
	uint32_t number_of_locals;
	// list of verification objects;
	RList* local_items;
	uint32_t number_of_stack_items;
	// list of verification objects;
	RList* stack_items;

} RBinJavaStackMapFrame;

typedef struct r_bin_java_source_debugging_extension_attr_t {
	uint8_t* debug_extension;
} RBinJavaSourceDebugExtensionAttr;

typedef struct r_bin_java_enclosing_method_attr_t {
	uint16_t class_idx;
	uint16_t method_idx;
	char *class_name;
	char *method_name;
	char *method_descriptor;
} RBinJavaEnclosingMethodAttr;

typedef struct r_bin_java_boot_strap_arg_t{
	uint64_t file_offset;
	uint64_t size;
	uint16_t argument_info_idx;
	// additional informations?
	RBinJavaCPTypeObj *argument_info_cp_obj;
} RBinJavaBootStrapArgument;

typedef struct r_bin_java_boot_strap_method_t{
	uint64_t file_offset;
	uint64_t size;
	uint16_t bootstrap_method_ref;
	uint16_t num_bootstrap_arguments;
	// List of RBinJavaCodeAttribute
	RList *bootstrap_arguments;
} RBinJavaBootStrapMethod;


typedef struct r_bin_java_boot_strap_methods_t{
	uint16_t num_bootstrap_methods;
	RList *bootstrap_methods;
} RBinJavaBootstrapMethodsAttr;


typedef struct {
	uint16_t type_name_idx;
	uint16_t const_name_idx;
	RBinJavaCPTypeObj *const_name_cp_obj;
	RBinJavaCPTypeObj *type_name_cp_obj;
} RBinJavaEnumConstValue;

typedef struct {
	uint16_t const_value_idx;
	RBinJavaCPTypeObj *const_value_cp_obj;
} RBinJavaConstValue;

typedef struct {
	uint16_t class_info_idx;
	RBinJavaCPTypeObj *class_info_cp_obj;;
} RBinJavaClassInfoValue;

typedef struct r_bin_java_element_value_ary_t{
	uint64_t size;
	uint32_t num_values;
	RList *values;
} RBinJavaElementValueArray;



typedef struct r_bin_java_annotation_t{
	uint64_t size;
	uint16_t type_idx;
	uint16_t num_element_value_pairs;
	RList *element_value_pairs;
} RBinJavaAnnotation;

typedef enum {
	// Primitive Types
	R_BIN_JAVA_EV_TAG_BYTE = 'B',
	R_BIN_JAVA_EV_TAG_CHAR = 'C',
	R_BIN_JAVA_EV_TAG_DOUBLE = 'D',
	R_BIN_JAVA_EV_TAG_FLOAT = 'F',
	R_BIN_JAVA_EV_TAG_INT = 'I',
	R_BIN_JAVA_EV_TAG_LONG = 'J',
	R_BIN_JAVA_EV_TAG_SHORT = 'S',
	R_BIN_JAVA_EV_TAG_BOOLEAN = 'Z',
	// Other tags
	R_BIN_JAVA_EV_TAG_ARRAY = '[',
	R_BIN_JAVA_EV_TAG_STRING = 's',
	R_BIN_JAVA_EV_TAG_ENUM = 'e',
	R_BIN_JAVA_EV_TAG_CLASS = 'c',
	R_BIN_JAVA_EV_TAG_ANNOTATION = '@',
	//	R_BIN_JAVA_EV_TAG_CLASSNAME = 'L',
	R_BIN_JAVA_EV_TAG_UNKNOWN = 0xff,
} R_BIN_JAVA_EV_TAG;

typedef struct r_bin_java_element_value_t {
	RBinJavaMetaInfo *metas;
	uint64_t size;
	uint8_t tag;
	uint64_t file_offset;
	union {
		RBinJavaConstValue const_value;
		RBinJavaEnumConstValue enum_const_value;
		RBinJavaClassInfoValue class_value;
		RBinJavaAnnotation annotation_value;
		RBinJavaElementValueArray array_value;
	} value;
} RBinJavaElementValue;

typedef struct r_bin_java_element_value_pair_t{
	uint64_t file_offset;
	uint64_t size;
	uint16_t element_name_idx;
	char* name;
	RBinJavaElementValue *value;
} RBinJavaElementValuePair;



typedef struct r_bin_java_annotations_attr_t {
	uint64_t size;
	uint16_t num_annotations;
	RList *annotations;
} RBinJavaAnnotationsArray;

typedef RBinJavaAnnotationsArray RBinJavaRuntimeVisibleAnnotationsAttr;
typedef RBinJavaAnnotationsArray RBinJavaRuntimeInvisibleAnnotationsAttr;
typedef RBinJavaAnnotationsArray RBinJavaRuntimeParameterAnnotationsArray;

typedef struct r_bin_java_parameter_annotation_attr_t {
	uint8_t num_parameters;
	// RBinJavaRuntimeParameterAnnotationsArray
	RList *parameter_annotations;
} RBinJavaRuntimeVisibleParameterAnnotationsAttr;

typedef RBinJavaRuntimeVisibleParameterAnnotationsAttr RBinJavaRuntimeInvisibleParameterAnnotationsAttr;

typedef struct r_bin_java_parameter_annotations_attr_t {
	RBinJavaElementValue *default_value;
} RBinJavaAnnotationDefaultAttr;

typedef struct r_bin_java_stack_map_table_attr_t { // attribute StackMap
	uint32_t code_size;
	uint32_t number_of_entries;
	RList* stack_map_frame_entries;
} RBinJavaStackMapTableAttr;


typedef struct r_bin_java_signature_attr_t {
	uint16_t signature_idx;
	char *signature;
} RBinJavaSignatureAttr;

typedef struct r_bin_java_stack_verification_t{
	uint64_t file_offset;
	uint64_t size;
	uint8_t tag;
	char *name;
	union {
		uint16_t obj_val_cp_idx;
		uint16_t uninit_offset;
	} info;
} RBinJavaVerificationObj;

typedef struct r_bin_java_fm_t {
	RBinJavaMetaInfo *metas;
	uint64_t size;
	char *name;
	char *descriptor;
	char *class_name;
	char *flags_str;

	uint64_t file_offset;
	R_BIN_JAVA_FIELD_TYPE type;
	uint16_t flags;
	uint16_t name_idx;
	uint16_t descriptor_idx;

	RBinJavaCPTypeObj *field_ref_cp_obj;
	uint64_t attr_offset;
	uint16_t attr_count;
	RList *attributes;
	uint64_t method_number;
} RBinJavaField;

typedef struct r_bin_java_interface_info_desc_t{
	char *name;
	uint64_t size;
	uint64_t file_offset;
	uint16_t class_info_idx;
	RBinJavaCPTypeObj *cp_class;
} RBinJavaInterfaceInfo;

typedef struct r_bin_java_attr_constant_t {
	uint16_t constantvalue_idx;
} RBinJavaConstantValueAttr;

typedef struct r_bin_java_attr_exceptions_t {
	uint16_t number_of_exceptions;
	uint16_t* exception_idx_table;
} RBinJavaExceptionsAttr;

typedef struct r_bin_java_attr_exception_table_entry_t {
	uint64_t file_offset;
	uint16_t start_pc;
	uint16_t end_pc;
	uint16_t handler_pc;
	uint16_t catch_type;
	uint64_t size;
} RBinJavaExceptionEntry;

typedef struct r_bin_java_attr_code_t {
	uint64_t file_offset;
	uint32_t code_offset;
	uint16_t max_stack;
	uint16_t max_locals;
	uint16_t code_length;
	uint8_t *code;

	uint32_t exception_table_length;
	RList *exception_table; // RBinJavaExceptionTableEntry *

	uint16_t attributes_count;
	RList *attributes;
	RBinJavaStackMapFrame *implicit_frame;
} RBinJavaCodeAttr;

typedef struct r_bin_java_attr_inner_classes_t {
	uint16_t number_of_classes;
	RList* classes;
} RBinJavaInnerClassesAttribute;

typedef struct r_bin_java_attr_source_file_t{
	uint16_t sourcefile_idx;
} RBinJavaSourceFileAttribute;

typedef struct r_bin_java_line_number_table_t{
	uint64_t file_offset;
	uint16_t start_pc;
	uint16_t line_number;
	uint64_t size;
} RBinJavaLineNumberAttribute;

typedef struct r_bin_java_attr_linenum_t {
	uint64_t file_offset;
	uint16_t line_number_table_length;
	RList* line_number_table; // RBinJavaLineNumberTable*
} RBinJavaLineNumberTableAttribute;

typedef struct r_bin_java_attr_localvariabletype_t{
	char *name;
	char *signature;

	uint64_t file_offset;
	uint16_t start_pc;
	uint16_t length;
	uint16_t name_idx;
	uint16_t signature_idx;
	uint16_t index;
	uint64_t size;
} RBinJavaLocalVariableTypeAttribute;

typedef struct r_bin_java_attr_localvariable_type_table_t {
	uint16_t table_length;
	RList* local_variable_table; // RBinJavaLocalVariable
} RBinJavaLocalVariableTypeTableAttribute;

typedef struct r_bin_java_attr_localvariable_t{
	char *name;
	char *descriptor;

	uint64_t file_offset;
	uint16_t start_pc;
	uint16_t length;
	uint16_t name_idx;
	uint16_t descriptor_idx;
	uint16_t index;
	uint64_t size;
} RBinJavaLocalVariableAttribute;


typedef struct r_bin_java_attr_localvariable_table_t {
	uint16_t table_length;
	RList* local_variable_table; // RBinJavaLocalVariable
} RBinJavaLocalVariableTableAttribute;

typedef struct r_bin_java_attr_t {
	uint8_t *bytes;
	uint64_t pos;
	uint64_t size;
	char *name;
	uint64_t file_offset;
	RBinJavaMetaInfo *metas;
	int type;
	uint16_t name_idx; //	uint16_t attribute_name_idx;
	uint32_t length;   //uint16_t attribute_length;
	uint64_t loadaddr;
	union {
		RBinJavaAnnotationDefaultAttr annotation_default_attr;
		RBinJavaBootstrapMethodsAttr bootstrap_methods_attr;
		RBinJavaCodeAttr code_attr;
		RBinJavaConstantValueAttr constant_value_attr;
		RBinJavaEnclosingMethodAttr enclosing_method_attr;
		RBinJavaExceptionsAttr exceptions_attr;
		RBinJavaLineNumberTableAttribute line_number_table_attr;
		RBinJavaLocalVariableTableAttribute local_variable_table_attr;
		RBinJavaLocalVariableTypeTableAttribute local_variable_type_table_attr;
		RBinJavaInnerClassesAttribute inner_classes_attr;

		RBinJavaAnnotationsArray annotation_array;
		RBinJavaRuntimeVisibleAnnotationsAttr rtv_annotations_attr;
		RBinJavaRuntimeInvisibleAnnotationsAttr rti_annotations_attr;

		RBinJavaRuntimeVisibleParameterAnnotationsAttr rtvp_annotations_attr;
		RBinJavaRuntimeInvisibleParameterAnnotationsAttr rtip_annotations_attr;
		RBinJavaSourceDebugExtensionAttr debug_extensions;
		RBinJavaSourceFileAttribute source_file_attr;
		RBinJavaStackMapTableAttr stack_map_table_attr;
		RBinJavaSignatureAttr signature_attr;

	} info;

} RBinJavaAttrInfo;

typedef struct r_bin_java_attr_classes_t {
	char *name;
	char *flags_str;
	uint64_t file_offset;
	RBinJavaAttrInfo *clint_attr;
	RBinJavaField *clint_field;
	uint16_t inner_class_info_idx;
	uint16_t outer_class_info_idx;
	uint16_t inner_name_idx;
	uint16_t inner_class_access_flags;
	uint64_t size;
} RBinJavaClassesAttribute;

typedef struct r_bin_java_classfile_t {
	uint8_t cafebabe[4];
	uint8_t minor[2];
	uint8_t major[2];
	uint16_t cp_count;
} RBinJavaClass;

typedef struct r_bin_java_classfile2_t {
	uint16_t access_flags;
	char *flags_str;
	char *this_class_name;
	uint16_t this_class;
	uint16_t super_class;
	uint16_t cf2_size;
	RBinJavaField *this_class_entrypoint;
	RBinJavaAttrInfo *this_class_entrypoint_code_attr;
} RBinJavaClass2;

typedef struct r_bin_java_lines_t {
	int count;
	int *addr;
	int *line;
} RBinJavaLines;

typedef struct r_bin_java_obj_t {
	struct r_bin_java_classfile_t cf;
	RBinJavaClass2 cf2;

	uint64_t cp_offset, fields_offset, interfaces_offset;
	uint64_t classes_offset, methods_offset, attrs_offset;
	uint32_t cp_size, cp_count;
	uint32_t fields_size, fields_count;
	uint32_t interfaces_size, interfaces_count;
	uint32_t methods_size, methods_count;
	uint32_t classes_size, classes_count;
	uint32_t attrs_size, attrs_count;

	uint64_t loadaddr; // load address that is used to calc actual offset
				// when multiple bins are loaded at once
	int size;
	int calc_size;
	char* file;
	RBinJavaLines lines;

	// These state variables are used for parsing the appropriate number of bytes
	// when readin uoffset, ustack, ulocalvar values
	uint8_t ulocalvar_sz;
	uint8_t ustack_sz;
	uint8_t offset_sz;
	uint32_t cur_method_code_length;
	RBinJavaAttrInfo *current_code_attr;

	uint32_t attr_idx;
	uint32_t method_idx;
	uint32_t field_idx;
	uint32_t cp_idx;
	uint32_t interface_idx;
	uint32_t attributes_idx;

	//uint32_t classes_idx; //TODO: when classes list is being used, update this value

	int fsym;
	int fsymsz;

	RBinJavaField *main;
	RBinJavaAttrInfo *main_code_attr;

	RBinJavaField *entrypoint;
	RBinJavaAttrInfo *entrypoint_code_attr;

	//RList* classes_list; // TODO: Not sure if this is necessary.  it would be the inner classes info.
	RList* fields_list;
	RList* methods_list;
	RList* cp_list;
	RList* interfaces_list;
	RList* attrs_list;
	RList* imports_list;

	RList* functions;
	RList* disassembly;

	//Sdb *kv;
	//Sdb *AllJavaBinObjs;
	uint32_t id;
} RBinJavaObj;

RList * r_bin_java_get_interface_names(RBinJavaObj * bin);
RBinJavaCPTypeObj* r_bin_java_get_item_from_cp(RBinJavaObj *bin, int i);
RBinJavaCPTypeObj* r_bin_java_get_item_from_bin_cp_list(RBinJavaObj *bin, uint64_t idx);
uint8_t * r_bin_java_cp_get_idx_bytes(RBinJavaObj *bin, uint16_t idx, uint32_t *out_sz);
RList * r_bin_java_get_lib_names(RBinJavaObj * bin);
RList* r_bin_java_get_sections(RBinJavaObj *bin);
RList* r_bin_java_get_fields(RBinJavaObj *bin);
char* r_bin_java_get_version(RBinJavaObj* bin);
RBinAddr * r_bin_java_get_entrypoint(RBinJavaObj* bin, int sym);
RList* r_bin_java_get_entrypoints(RBinJavaObj* bin);
uint64_t r_bin_java_get_main(RBinJavaObj* bin);
RList* r_bin_java_get_symbols(RBinJavaObj* bin);
RList* r_bin_java_get_strings(RBinJavaObj* bin);
void* r_bin_java_free(RBinJavaObj* bin);
RBinJavaObj* r_bin_java_new(const char* file, uint64_t baddr);//, Sdb * kv);
RBinJavaObj* r_bin_java_new_buf(uint8_t* buf, uint64_t sz, uint64_t baddr);//, Sdb * kv);
int r_bin_java_valid_class (const uint8_t * buf, uint64_t buf_sz);

// Stuff used to manage Java Class File Constant Information
typedef struct r_bin_java_object_allocs_t {
	RBinJavaCPTypeObj *(*new_obj) (RBinJavaObj *bin, uint8_t* buffer, uint64_t offset) ;
	void (*delete_obj) (void /*RBinJavaCPTypeObj*/ *obj);
	void (*print_summary) (RBinJavaCPTypeObj *obj);
	uint64_t (*calc_size) (RBinJavaCPTypeObj *obj);
	char* (*stringify_obj) (RBinJavaCPTypeObj *obj);
} RBinJavaCPTypeObjectAllocs;

typedef struct r_bin_java_attr_allocs_t {
	//void (*new_obj) (RBinJavaObj *bin, RBinJavaAttrInfo *obj, uint64_t offset) ;
	RBinJavaAttrInfo* (*new_obj)(uint8_t* buffer, uint64_t sz, uint64_t buf_offset);
	void (*delete_obj) (void /*RBinJavaAttrInfo*/ *obj);
	void (*print_summary) (RBinJavaAttrInfo *obj);
	uint64_t (*calc_size)(RBinJavaAttrInfo *obj);
} RBinJavaAttrInfoObjectAllocs;

typedef struct r_bin_java_ver_allocs_t {
	void (*new_obj) (RBinJavaObj *bin, uint32_t code_length, uint64_t offset) ;
	void (*delete_obj) (void /*RBinJavaAttrInfo*/ *obj);
	void (*print_summary) (RBinJavaAttrInfo *obj);
} RBinJavaVerInfoObjectAllocs;

typedef struct r_bin_java_stack_frame_allocs_t {
	RBinJavaStackMapFrame* (*new_obj) (RBinJavaObj *bin, uint64_t offset) ;
	void (*delete_obj) (void /* RBinJavaStackMapFrame*/ *obj);
	void (*print_summary) (RBinJavaStackMapFrame *obj);
} RBinJavaStackMapFrameObjectAllocs;

typedef struct {
	RBinJavaElementValue* (*new_obj) (RBinJavaObj *bin, uint64_t offset) ;
	void (*delete_obj) (void /*RBinJavaElementValue*/ *obj);
	void (*print_summary) (RBinJavaElementValue *obj);
} RBinJavaElementValueObjectAllocs;

typedef struct r_bin_R_BIN_JAVA_CP_ant_meta_t {
	char *name;
	uint8_t tag;
	uint32_t len;
	RBinJavaCPTypeObjectAllocs *allocs;
} RBinJavaCPTypeMetas;

typedef struct r_bin_java_attr_meta_t {
	char *name;
	uint8_t type;
	RBinJavaAttrInfoObjectAllocs *allocs;
} RBinJavaAttrMetas;

typedef struct r_bin_java_ver_meta_t {
	char *name;
	uint8_t type;
} RBinJavaVerificationMetas;

typedef struct r_bin_java_stack_frame_meta_t {
	char *name;
	uint8_t type;
	RBinJavaStackMapFrameObjectAllocs *allocs;
} RBinJavaStackMapFrameMetas;

typedef struct{
	char* name;
	uint8_t tag;
	RBinJavaElementValueObjectAllocs* allocs;
} RBinJavaElementValueMetas;

typedef struct{
	char* name;
	uint8_t tag;
} RBinJavaRefMetas;


typedef struct java_const_value_str_t {
	uint32_t len;
	char *str;
} _JavaStr;

typedef struct java_const_value_ref_t {
	char *class_name;
	char *name;
	char *desc;
	uint8_t is_method;
	uint8_t is_field;
} _JavaRef;


typedef struct java_const_value_t {
	const char * type;
	union {
		uint8_t _null;
		uint8_t _char;
		uint8_t _byte;
		uint64_t _long;
		double _double;
		uint32_t _int;
		float _float;
		uint16_t _short;
		uint8_t _bool;
		_JavaStr * _str;
		_JavaRef * _ref;
	} value;
} ConstJavaValue;

uint8_t* r_bin_java_get_attr_buf(RBinJavaObj *bin,  uint64_t sz, const uint64_t offset, const uint8_t *buf, const uint64_t len);
char* r_bin_java_get_name_from_cp_item_list(RList *cp_list, uint64_t idx);
char* r_bin_java_get_utf8_from_cp_item_list(RList *cp_list, uint64_t idx);
uint32_t r_bin_java_get_utf8_len_from_cp_item_list(RList *cp_list, uint64_t idx);
char* r_bin_java_get_desc_from_cp_item_list(RList *cp_list, uint64_t idx);
char* r_bin_java_get_item_name_from_cp_item_list(RList *cp_list, RBinJavaCPTypeObj *obj);
char* r_bin_java_get_item_desc_from_cp_item_list(RList *cp_list, RBinJavaCPTypeObj *obj);



char* r_bin_java_get_name_from_bin_cp_list(RBinJavaObj *bin, uint64_t idx);
char* r_bin_java_get_utf8_from_bin_cp_list(RBinJavaObj *bin, uint64_t idx);
uint32_t r_bin_java_get_utf8_len_from_bin_cp_list(RBinJavaObj *bin, uint64_t idx);
char* r_bin_java_get_desc_from_bin_cp_list(RBinJavaObj *bin, uint64_t idx);
char* r_bin_java_get_item_name_from_bin_cp_list(RBinJavaObj *bin, RBinJavaCPTypeObj *obj);
char* r_bin_java_get_item_desc_from_bin_cp_list(RBinJavaObj *bin, RBinJavaCPTypeObj *obj);

// free bin lists

char * r_bin_java_print_utf8_cp_stringify(RBinJavaCPTypeObj* obj);
char * r_bin_java_print_name_and_type_cp_stringify(RBinJavaCPTypeObj* obj);
char * r_bin_java_print_double_cp_stringify(RBinJavaCPTypeObj* obj);
char * r_bin_java_print_long_cp_stringify(RBinJavaCPTypeObj* obj);
char * r_bin_java_print_float_cp_stringify(RBinJavaCPTypeObj* obj);
char * r_bin_java_print_integer_cp_stringify(RBinJavaCPTypeObj* obj);
char * r_bin_java_print_string_cp_stringify(RBinJavaCPTypeObj* obj);
char * r_bin_java_print_classref_cp_stringify(RBinJavaCPTypeObj* obj);
char * r_bin_java_print_fieldref_cp_stringify(RBinJavaCPTypeObj* obj);
char * r_bin_java_print_methodref_cp_stringify(RBinJavaCPTypeObj* obj);
char * r_bin_java_print_interfacemethodref_cp_stringify(RBinJavaCPTypeObj* obj);
char * r_bin_java_print_unknown_cp_stringify(RBinJavaCPTypeObj* obj);
char * r_bin_java_print_null_cp_stringify(RBinJavaCPTypeObj* obj);
char * r_bin_java_print_methodtype_cp_stringify(RBinJavaCPTypeObj* obj);
char * r_bin_java_print_invokedynamic_cp_stringify(RBinJavaCPTypeObj* obj);
char * r_bin_java_print_methodhandle_cp_stringify(RBinJavaCPTypeObj* obj);
char * r_bin_java_print_class_cp_stringify(RBinJavaCPTypeObj* obj);


RBinSymbol* r_bin_java_create_new_symbol_from_field_with_access_flags(RBinJavaField *fm_type);
RBinSymbol* r_bin_java_create_new_symbol_from_cp_idx(uint32_t cp_idx, uint64_t baddr);
RBinSymbol* r_bin_java_create_new_symbol_from_invoke_dynamic(RBinJavaCPTypeObj *obj, uint64_t baddr);
RBinSymbol* r_bin_java_create_new_symbol_from_ref(RBinJavaCPTypeObj *obj, uint64_t baddr);
RBinSymbol* r_bin_java_create_new_symbol_from_method(RBinJavaField *fm_type);

uint64_t r_bin_java_get_method_code_offset(RBinJavaField *fm_type);
uint64_t r_bin_java_get_method_code_size(RBinJavaField *fm_type);
uint64_t r_bin_java_get_class_entrypoint(RBinJavaObj* bin);

RBinJavaCPTypeObj *r_bin_java_find_cp_ref_info(RBinJavaObj *bin, uint16_t name_and_typeidx);
RBinJavaCPTypeObj *r_bin_java_find_cp_ref_info_from_name_and_type(RBinJavaObj *bin, uint16_t name_idx, uint16_t descriptor_idx);
RBinJavaCPTypeObj *r_bin_java_find_cp_name_and_type_info(RBinJavaObj *bin, uint16_t name_idx, uint16_t descriptor_idx);

RBinClass* r_bin_java_allocate_r_bin_class();
RList *r_bin_java_get_classes(RBinJavaObj *bin);
RList *r_bin_java_enum_class_methods(RBinJavaObj *bin, uint16_t class_idx);
RList *r_bin_java_enum_class_fields(RBinJavaObj *bin, uint16_t class_idx);
uint64_t r_bin_java_find_method_offset(RBinJavaObj *bin, const char* method_name);

RBinJavaField * r_bin_java_get_method_code_attribute_with_addr(RBinJavaObj *bin, uint64_t addr);
RList * r_bin_java_get_method_exception_table_with_addr(RBinJavaObj *bin, uint64_t addr);

const RList* r_bin_java_get_methods_list(RBinJavaObj* bin);
const RBinJavaObj* r_bin_java_get_bin_obj(const char *name);
int r_bin_java_update_file (const char *key, RBinJavaObj *bin_obj);
RBinJavaObj * r_bin_java_get_sdb_bin_obj(const char * filename);
RList * r_bin_java_get_bin_obj_list();
RList* r_bin_java_get_bin_obj_list_thru_obj(RBinJavaObj *bin_obj);
char * r_bin_java_get_this_class_name(RBinJavaObj *bin_obj);
char * r_bin_java_build_obj_key (RBinJavaObj *bin);

RList * r_bin_java_extract_type_values(const char *arg_str);
int r_bin_java_extract_reference_name(const char * input_str, char ** ref_str, uint8_t array_cnt);
RList * r_bin_java_extract_all_bin_type_values( RBinJavaObj * bin_obj);

RList * r_bin_java_get_method_definitions(RBinJavaObj *bin);
char * r_bin_java_get_method_definition(RBinJavaField *fm_type);
RList * r_bin_java_get_field_definitions(RBinJavaObj *bin);
char * r_bin_java_get_field_definition(RBinJavaField *fm_type);
RList * r_bin_java_get_import_definitions(RBinJavaObj *bin);
RList * r_bin_java_get_field_offsets(RBinJavaObj *bin);
RList * r_bin_java_get_method_offsets(RBinJavaObj *bin);

uint16_t r_bin_java_calculate_method_access_value(const char * access_flags_str);
uint16_t r_bin_java_calculate_field_access_value(const char * access_flags_str);
uint16_t r_bin_java_calculate_class_access_value(const char * access_flags_str);

RList * retrieve_all_method_access_string_and_value();
RList * retrieve_all_field_access_string_and_value();
RList * retrieve_all_class_access_string_and_value();
char * retrieve_method_access_string(uint16_t flags);
char * retrieve_field_access_string(uint16_t flags);
char * retrieve_class_method_access_string(uint16_t flags);


char * r_bin_java_resolve(RBinJavaObj *obj, int idx, uint8_t space_bn_name_type);
char * r_bin_java_resolve_with_space(RBinJavaObj *obj, int idx);
char * r_bin_java_resolve_without_space(RBinJavaObj *BIN_OBJ, int idx);
char * r_bin_java_resolve_cp_idx_type(RBinJavaObj *BIN_OBJ, int idx);
char * r_bin_java_resolve_b64_encode(RBinJavaObj *BIN_OBJ, uint16_t idx);
uint64_t r_bin_java_resolve_cp_idx_address(RBinJavaObj *BIN_OBJ, int idx);
char * r_bin_java_resolve_cp_idx_to_string(RBinJavaObj *BIN_OBJ, int idx);
int r_bin_java_resolve_cp_idx_print_summary(RBinJavaObj *BIN_OBJ, int idx);

struct java_const_value_t * r_bin_java_resolve_to_const_value(RBinJavaObj *BIN_OBJ, int idx);
void r_bin_java_free_const_value(struct java_const_value_t * cp_value);


char * r_bin_java_get_fcn_name ( RBinJavaField *fm_type);
RList * r_bin_java_get_args ( RBinJavaField *fm_type);
RList * r_bin_java_get_ret ( RBinJavaField *fm_type);

RList * r_bin_java_get_args_from_bin(RBinJavaObj *bin_obj, uint64_t addr);
RList * r_bin_java_get_ret_from_bin(RBinJavaObj *bin_obj, uint64_t addr);
char * r_bin_java_get_fcn_name_from_bin( RBinJavaObj *bin_obj, uint64_t addr);
int r_bin_java_is_method_static(RBinJavaObj *bin_obj, uint64_t addr);

uint8_t r_bin_java_does_cp_idx_ref_method(RBinJavaObj *BIN_OBJ, int idx);
uint8_t r_bin_java_does_cp_idx_ref_field(RBinJavaObj *BIN_OBJ, int idx);
int r_bin_java_is_method_protected(RBinJavaObj *bin_obj, uint64_t addr);
int r_bin_java_is_method_private(RBinJavaObj *bin_obj, uint64_t addr);
RBinJavaAttrInfo* r_bin_java_get_method_code_attribute(const RBinJavaField *method);

char * r_bin_java_get_method_name(RBinJavaObj *bin_obj, uint32_t idx);
uint64_t r_bin_java_get_method_code_start(RBinJavaObj *bin_obj, uint32_t idx);
uint8_t * r_bin_java_get_method_code (RBinJavaObj *bin_obj, uint32_t idx);
uint64_t r_bin_java_get_method_code_len (RBinJavaObj *bin_obj, uint32_t idx);
int r_bin_java_print_method_idx_summary (RBinJavaObj *bin_obj, uint32_t idx);
uint32_t r_bin_java_get_method_count(RBinJavaObj *bin_obj);
RList * r_bin_java_get_method_num_name ( RBinJavaObj *bin_obj);

char * r_bin_java_get_field_name ( RBinJavaObj *bin_obj, uint32_t idx);
int r_bin_java_print_field_idx_summary (RBinJavaObj *bin_obj, uint32_t idx);
uint32_t r_bin_java_get_field_count (RBinJavaObj *bin_obj);
RList * r_bin_java_get_field_num_name (RBinJavaObj *bin_obj);

RList * r_bin_java_find_cp_const_by_val ( RBinJavaObj *bin_obj, const uint8_t *bytes, uint32_t len, const char t);
char r_bin_java_resolve_cp_idx_tag(RBinJavaObj *BIN_OBJ, int idx);

int r_bin_java_integer_cp_set(RBinJavaObj *bin, uint16_t idx, uint32_t val );
int r_bin_java_float_cp_set(RBinJavaObj *bin, uint16_t idx, float val );
int r_bin_java_long_cp_set(RBinJavaObj *bin, uint16_t idx, uint64_t val );
int r_bin_java_double_cp_set(RBinJavaObj *bin, uint16_t idx, uint32_t val );
int r_bin_java_utf8_cp_set(RBinJavaObj *bin, uint16_t idx, const uint8_t* buffer, uint32_t len);
uint8_t * r_bin_java_cp_get_bytes(uint8_t tag, uint32_t *out_sz, const uint8_t *buf, const uint64_t len);
uint8_t * r_bin_java_cp_idx_get_bytes(RBinJavaObj *bin, uint16_t idx, uint32_t *out_sz);
uint32_t r_bin_java_cp_get_size(RBinJavaObj *bin, uint16_t idx);

uint64_t r_bin_java_parse_cp_pool (RBinJavaObj *bin, const uint64_t offset, const uint8_t * buf, const uint64_t len);
uint64_t r_bin_java_parse_interfaces (RBinJavaObj *bin, const uint64_t offset, const uint8_t * buf, const uint64_t len);
uint64_t r_bin_java_parse_fields (RBinJavaObj *bin, const uint64_t offset, const uint8_t * buf, const uint64_t len);
uint64_t r_bin_java_parse_methods (RBinJavaObj *bin, const uint64_t offset, const uint8_t * buf, const uint64_t len);
uint64_t r_bin_java_parse_attrs (RBinJavaObj *bin, const uint64_t offset, const uint8_t * buf, const uint64_t len);
int r_bin_java_load_bin (RBinJavaObj *bin, const uint8_t * buf, uint64_t len);
void r_bin_add_import (RBinJavaObj * bin, RBinJavaCPTypeObj *cp_obj, const char * type);
void r_bin_java_set_imports(RBinJavaObj* bin);
RList* r_bin_java_get_imports(RBinJavaObj* bin);

uint64_t r_bin_java_get_method_start(RBinJavaObj *bin, RBinJavaField *method);
uint64_t r_bin_java_get_method_end(RBinJavaObj *bin, RBinJavaField *method);

uint8_t * r_bin_java_cp_get_fref_bytes (RBinJavaObj *bin, uint32_t *out_sz, uint8_t tag, uint16_t cn_idx, uint16_t fn_idx, uint16_t ft_idx );
uint8_t * r_bin_java_cp_append_method_ref (RBinJavaObj *bin, uint32_t *out_sz, uint16_t cn_idx, uint16_t fn_idx, uint16_t ft_idx );
uint8_t * r_bin_java_cp_append_field_ref(RBinJavaObj *bin, uint32_t *out_sz, uint16_t cn_idx, uint16_t fn_idx, uint16_t ft_idx );
char * r_bin_java_unmangle_without_flags (const char *name, const char *descriptor);
char * r_bin_java_unmangle (const char *flags, const char *name, const char *descriptor);

struct basic_json_t /*DsoJsonObj*/ * r_bin_java_get_field_json_definitions(RBinJavaObj *bin);
struct basic_json_t /*DsoJsonObj*/ * r_bin_java_get_method_json_definitions(RBinJavaObj *bin);
struct basic_json_t /*DsoJsonObj*/ * r_bin_java_get_import_json_definitions(RBinJavaObj *bin);
struct basic_json_t /*DsoJsonObj*/ * r_bin_java_get_interface_json_definitions(RBinJavaObj *bin);

struct basic_json_t /*DsoJsonObj*/ * r_bin_java_get_fm_type_definition_json(RBinJavaObj *bin, RBinJavaField *fm_type, int is_method);
struct basic_json_t /*DsoJsonObj*/ * r_bin_java_get_field_json_definition(RBinJavaObj *bin, RBinJavaField *fm_type);
struct basic_json_t /*DsoJsonObj*/ * r_bin_java_get_method_json_definition(RBinJavaObj *bin, RBinJavaField *fm_type);
struct basic_json_t /*DsoJsonObj*/ * r_bin_java_get_class_info_json(RBinJavaObj *bin);

struct basic_json_t /*DsoJsonObj*/ * r_bin_java_get_bin_obj_json (RBinJavaObj *bin);
uint64_t r_bin_java_calc_class_size(const uint8_t* bytes, uint64_t size);
int r_bin_java_valid_class (const uint8_t * buf, uint64_t buf_sz);
char * r_bin_java_hexlify (const uint8_t * buf, uint64_t buf_sz);
#endif
