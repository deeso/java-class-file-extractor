//Copyright 2015 Adam Pridgen
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

#include "class.h"
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

//gcc -ggdb3 -c -Wall -fPIC class.c  > /dev/null 2> grep error
//gcc -ggdb3 -c -Wall -fPIC dsojson.c  > /dev/null 2> grep error
//gcc -ggdb3 -shared -fPIC -o libjanalysis.so class.o dsojson.o
//gcc -ggdb3 class_extract.c  -ggdb3 -L. -ljanalysis -o class_extract
//gcc class_sz.c  -ggdb3 -L. -ljanalysis -o class_sz


const char * SEP = "//";
int is_start_class (const uint8_t *b, uint64_t pos, uint64_t sz);
uint64_t extract_class_name (char * filename, uint64_t offset, uint64_t sz, char *dir, char *name);
uint64_t find_next_potential_cafebabe (const uint8_t *buf, uint64_t off, uint64_t sz);

int is_start_class (const uint8_t *b, uint64_t pos, uint64_t sz) {
    const uint8_t *buf = pos+b;
    return pos+4 < sz && !memcmp (buf, "\xCA\xFE\xBA\xBE", 4);
}


int check_make_dir (const char *dir) {
    struct stat sb;
    int res = 0;
    if (stat(dir, &sb) == 0 && S_ISDIR(sb.st_mode)) {
        res = 1;
    } else {
        res = !mkdir(dir, 0700) ? 1 : 0;
    }
    return res;
}

int escape_name (char *name, int len) {

    int pos = 0, changed = 0;

    while (pos < len) {
        if (name[pos] == '/') {
            changed = 1;
            name[pos] = '_';
        }
        pos ++;
    }
    return changed;
}

uint64_t extract_class_name (char * filename, uint64_t offset, uint64_t sz, char *dir, char *name) {
    uint8_t *buf = sz > 0 && sz != -1 ? malloc (sz) : NULL;
    FILE * f = buf && filename ? fopen (filename, "rb") : NULL;
    uint32_t len;
    char *outname = NULL;
    char *dst = NULL;

    if (!filename || !name) {
        free (buf);
        return -1;
    }

    len = strlen (name) + 20;
    outname = malloc (len);
    snprintf (outname, len, "%s_%08lx", name, offset);
    escape_name (outname, strlen(outname));

    fseek (f, offset, SEEK_SET);
    fread (buf, 1, sz, f);
    fclose(f);

    if (dir && check_make_dir (dir)) {
        len = strlen (dir) + strlen (outname) + 4;
        dst = malloc (len);
        snprintf (dst, len, "%s%s%s", dir, SEP, outname);
    } else {
        len = strlen (outname) + 4;
        dst = malloc (len);
        snprintf (dst, len, "%s", outname);
    }

    f = fopen (dst, "wb");
    fwrite (buf, 1, sz, f);
    fclose (f);

    free (buf);
    free (dst);
    free (outname);

}

uint64_t find_next_potential_cafebabe (const uint8_t *buf, uint64_t off, uint64_t sz) {
    uint64_t res = -1;
    while (off < sz) {
        if (is_start_class (buf, off, sz) ) {
            res = off;
            break;
        }
        off += 1;
    }
    return res;
}


char ** extract_initial_method_bytes_output (RBinJavaObj *rbj, uint64_t base_offset, uint64_t offset, uint32_t num_bytes) {
    char *fmt = "class_name::%s||method_name::%s||method_sz::0x%04x||offset::0x%lx||virtual_base_offset::0x%lx||virtual_offset::0x%lx||bytes::%s\n";
    uint32_t num_methods = rbj ? r_bin_java_get_method_count (rbj)+1 : 1;
    char **res = rbj && num_methods ? malloc (sizeof (char *)* num_methods) : NULL;
    char *class_name = NULL;

    uint32_t idx = 0;
    if (!res) return NULL;
    if (num_methods <= 1){
        res[0] = NULL;
        return res;
    }
    class_name = r_bin_java_get_this_class_name (rbj);
    while (idx < num_methods-1) {
        char *method_name = r_bin_java_get_method_name (rbj, idx);
        uint64_t method_offset = r_bin_java_get_method_code_start (rbj, idx);
        uint64_t method_sz = r_bin_java_get_method_code_len (rbj, idx);
        uint8_t * code = r_bin_java_get_method_code (rbj, idx);
        char * hexified_bytes = r_bin_java_hexlify (code, (num_bytes > method_sz ? method_sz : num_bytes ) );
        uint32_t str_len = 150 + strlen(class_name) + strlen(method_name) + 16 + 16 + strlen (hexified_bytes) + 1;
        char * the_str = malloc (str_len);
        uint64_t virtual_offset = method_offset+offset+base_offset;
        snprintf (the_str, str_len, fmt, class_name, method_name, method_sz, method_offset+offset, base_offset, virtual_offset, hexified_bytes);
        free (method_name);
        free (code);
        free (hexified_bytes);

        res[idx] = the_str;
        idx++;
        res[idx] = NULL;
    }

    return res;
}




int main (int argc, char **argv) {
    char *filename = argc > 1 ? argv[1] : NULL;
    char *addr = argc > 2 ? argv[2] : NULL ;
    char *base_addr = argc > 3 ? argv[3] : NULL ;
    char *dst_dir = argc > 4 ? argv[4] : NULL ;
    uint32_t addr_len = filename && addr ? strlen (addr) : 0;
    uint32_t base_addr_len = filename && base_addr ? strlen (base_addr) : 0;
    int64_t offset = 0, base_offset = 0;
    uint8_t *buf = NULL;
    FILE * f = filename && addr ? fopen (filename, "rb") : NULL;
    uint64_t sz = 0, read_sz = 0;
    char *name = NULL;

    RBinJavaObj *rbj = NULL;

    if (!f || addr_len == 0 || !addr || !filename || !base_addr) {
        fprintf (stderr, "class_sz <filename> <offset> <base_offset> [dst_dir]\n");
        return -1;
    }

    if (addr[0] == '0' && (addr[1] == 'x' || addr[1] == 'X') && addr_len  > 2) {
        char * end = (addr+(int)addr_len);
        offset = strtoll (addr, &end, 16);
    } else {
        char * end = (addr+(int)addr_len);
        offset = strtoll (addr, &end, 10);
    }

    if (base_addr[0] == '0' && (base_addr[1] == 'x' || base_addr[1] == 'X') && base_addr_len  > 2) {
        char * end = (base_addr+(int)base_addr_len);
        base_offset = strtoll (base_addr, &end, 16);
    } else {
        char * end = (base_addr+(int)base_addr_len);
        base_offset = strtoll (base_addr, &end, 10);
    }

    fseek (f, 0, SEEK_END);
    sz = ftell (f);
    buf = malloc (sz);
    fseek (f, 0, SEEK_SET);
    read_sz = fread (buf, 1, sz, f);
    fclose(f);
    //fprintf (stderr, "[+] Scanning class Signature @ 0x%08lx\n", offset);
    while (offset < sz) {
        int64_t class_sz = 0;
        offset = find_next_potential_cafebabe (buf, offset, sz);

        if (offset == -1) break;

        fprintf (stderr, "[+] Found Java class Signature @ 0x%08lx\n", offset);
        class_sz = sz-offset >  0 && buf ? (int64_t) r_bin_java_calc_class_size (buf+offset, sz-offset) : -1;

        if (class_sz == -1 || class_sz == 0) {
            offset += 4;
            continue;
        } else if (class_sz > 0) {
            char *name = NULL;
            fprintf (stderr, "[+] Found Java Class @ 0x%08lx with size = %ld bytes\n", offset, class_sz);
            rbj = r_bin_java_new_buf (buf+offset, sz-offset, 0);
            if (rbj) {
                char ** class_code_info = NULL, **iter;
                name = r_bin_java_get_this_class_name(rbj);
                printf ("class_name::%s||filename::%s||offset::0x%lx||virtual_base_offset::0x%lx||class_sz::%ld\n", name, filename, offset, base_offset, class_sz);
                //fprintf (stderr, "class_name::%s||filename::%s||offset::0x%lx||base_offset::0x%lx||class_sz::%ld\n", name, filename, offset, base_offset,
                //class_sz);
                extract_class_name (filename, offset, class_sz, dst_dir, name);
                class_code_info = extract_initial_method_bytes_output (rbj, base_offset, offset, 10);
                iter = class_code_info;
                while (class_code_info && *iter != NULL) {
                    printf ("%s", *iter);
                    fprintf (stderr, "%s", *iter);
                    free (*iter);
                    iter++;
                }
                free (class_code_info);
                //printf ("Class name = %s\n", name);
                //free (name);
                free (name);
                offset += class_sz;
            } else {
                offset += 4;
            }
            r_bin_java_free (rbj);
            rbj = NULL;
        }
    }
    free (buf);
}
