//Copyright 2015 Adam Pridgen
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

#include "class.h"
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

//gcc -ggdb3 -c -Wall -fPIC class.c  > /dev/null 2> grep error
//gcc -ggdb3 -c -Wall -fPIC dsojson.c  > /dev/null 2> grep error
//gcc -ggdb3 -shared -fPIC -o libjanalysis.so class.o dsojson.o
//gcc class_sz.c  -ggdb3 -L. -ljanalysis -o class_sz


int is_start_class (uint8_t *b, uint64_t pos) {
	char *buf = pos+b;
	return buf[0] == '\xca' && buf[1] == '\xfe' && buf[2] == '\xba' && buf[3] == '\xbe';
}


int main (int argc, char **argv) {
	char *filename = argc > 2 ? argv[1] : NULL;
	char *addr = argc > 2 ? argv[2] : NULL ;
	uint32_t addr_len = filename && addr ? strlen (addr) : 0;
	int64_t class_sz = 0, offset = 0;
	uint8_t *buf = NULL;
	FILE * f = filename && addr ? fopen (filename, "rb") : NULL;
	uint64_t sz = 0, read_sz = 0;
	char *name = NULL;

	RBinJavaObj *rbj = NULL;

	if (addr_len == 0 || !addr || !filename) {
		printf ("class_sz <filename> <offset>\n");
		return -1;
	}

	if (addr[0] == '0' && (addr[1] == 'x' || addr[1] == 'X') && addr_len  > 2) {
		char * end = (addr+(int)addr_len);
		offset = strtoll (addr, &end, 16);
	} else {
		char * end = (addr+(int)addr_len);
		offset = strtoll (addr, &end, 10);
	}

	fseek (f, 0, SEEK_END);
	sz = ftell (f);
	buf = malloc (sz);
	fseek (f, 0, SEEK_SET);
	read_sz = fread (buf, 1, sz, f);
	fclose(f);



	class_sz = sz-offset >  0 && buf ? (int64_t) r_bin_java_calc_class_size (buf+offset, sz-offset) : -1;

	if (class_sz > 0 && class_sz != -1) {
		rbj = r_bin_java_new_buf (buf+offset, sz-offset, 0);
		if (rbj) {
			name = r_bin_java_get_this_class_name(rbj);
			//printf ("Class name = %s\n", name);
			//free (name);
		}
		r_bin_java_free (rbj);
		rbj = NULL;
	}

	if (class_sz > 0)
		printf ("class_name::%s||filename::%s||offset::0x%lx||class_sz::%ld\n", name, filename, offset, class_sz);
	free (buf);
	free (name);



}
