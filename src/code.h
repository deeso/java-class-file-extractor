#ifndef _INCLUDE_R_JAVA_H_
#define _INCLUDE_R_JAVA_H_

//#include <r_types.h>
#include "class.h"

typedef struct java_op {
	const char *name;
	unsigned char byte;
	int size;
	uint64_t op_type;
} JavaOp;

#ifndef R_IPI
#define R_IPI
#endif
#define JAVA_OPS_COUNT 297
R_IPI struct java_op JAVA_OPS[JAVA_OPS_COUNT];
R_IPI int java_print_opcode(RBinJavaObj *obj, uint64_t addr, int idx, const uint8_t *bytes, char *output, int outlen);
static int r_java_disasm(RBinJavaObj *obj, uint64_t addr, const uint8_t *bytes, char *output, int len);
static int r_java_assemble(uint8_t *bytes, const char *string);
//static void r_java_set_obj(RBinJavaObj *obj);
static void r_java_new_method ();

#endif
