//Copyright 2015 Adam Pridgen
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

#include "class.h"
#include "dsojson.h"
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

//gcc -ggdb3 -c -Wall -fPIC class.c  > /dev/null 2> grep error
//gcc -ggdb3 -c -Wall -fPIC dsojson.c  > /dev/null 2> grep error
//gcc -ggdb3 -shared -fPIC -o libjanalysis.so class.o dsojson.o
//gcc -ggdb3 class_extract.c  -ggdb3 -L. -ljanalysis -o class_extract
//gcc class_sz.c  -ggdb3 -L. -ljanalysis -o class_sz


const char * SEP = "//";
int is_start_class (const uint8_t *b, uint64_t pos, uint64_t sz);
uint64_t extract_class_name (char * filename, uint64_t offset, uint64_t sz, char *dir, char *name);
uint64_t find_next_potential_cafebabe (const uint8_t *buf, uint64_t off, uint64_t sz);

uint8_t  is_printable_char ( uint8_t b) {
    if (b < 0x20 || b > 0x7E) return 0;
    return 1;
}

uint32_t my_strlen (const uint8_t *buf, uint64_t offset, uint64_t sz) {
	char *b = (char *) buf + offset;
	uint32_t pos = 0;
	while (pos < sz && *b && is_printable_char (*b)) {
		pos++;
		b++;
	}
	return pos;
}

int is_start_class (const uint8_t *b, uint64_t pos, uint64_t sz) {
    const uint8_t *buf = pos+b;
    return pos+4 < sz && !memcmp (buf, "\xCA\xFE\xBA\xBE", 4);
}


int check_make_dir (const char *dir) {
    struct stat sb;
    int res = 0;
    if (stat(dir, &sb) == 0 && S_ISDIR(sb.st_mode)) {
        res = 1;
    } else {
        res = !mkdir(dir, 0700) ? 1 : 0;
    }
    return res;
}

int escape_name (char *name, int len) {

    int pos = 0, changed = 0;

    while (pos < len) {
        if (name[pos] == '/') {
            changed = 1;
            name[pos] = '_';
        }
        pos ++;
    }
    return changed;
}

uint64_t find_next_potential_cafebabe (const uint8_t *buf, uint64_t off, uint64_t sz) {
    uint64_t res = -1;
    while (off < sz) {
        if (is_start_class (buf, off, sz) ) {
            res = off;
            break;
        }
        off += 1;
    }
    return res;
}

int can_dword_be_ptr (const uint8_t *buf, uint64_t pos, uint64_t base_addr, uint64_t sz) {
    uint32_t v = R_BIN_JAVA_UINT (buf, pos);
    return base_addr <= v && v <= base_addr+sz;
}

uint64_t find_next_str (const uint8_t *buf, uint64_t offset, uint64_t sz, uint32_t min_len) {
    uint64_t pos = offset, str_start = 0;
    uint32_t str_len = 0;

    while (pos < sz) {
        char * c = (char *) buf+pos;
        str_len = 0;
        if (is_start_class (buf, pos, sz)) {
            uint64_t class_sz = sz-offset >  0 && buf ? (int64_t) r_bin_java_calc_class_size (buf+pos, sz-pos) : -1;
            if (class_sz > 0 && class_sz != -1) {
                pos += class_sz;
                continue;
            }
        }

        if (is_printable_char (*c)) {
            str_start = pos;

            while (pos < sz && *c && is_printable_char (*c) ) {
                pos++;
                str_len++;
                c = (char *) buf+pos;
            }

            if (str_len > min_len) {
                pos = str_start;
                break;
            }
        }

        pos++;
    }
    return pos;
}


uint32_t find_and_dump_cstrs (const char *filename, const uint8_t *buf, uint64_t offset, uint64_t base_addr,  uint64_t sz, uint32_t min_len) {
    uint32_t num_strs = 0;
    uint64_t pos = offset, next_str = -1;

    while (pos < sz) {
        char * the_str = NULL;
        uint64_t str_len = 0;
        uint64_t str_based_offset = 0;
        next_str = find_next_str (buf, pos, sz, min_len);
        if (next_str == -1 || next_str >= sz) break;

        str_len = my_strlen (buf, next_str, sz);
        the_str = dso_json_convert_string (buf+next_str, str_len);
        str_based_offset = base_addr + next_str;
        printf ("filename::%s||str::%s||offset::0x%lx||virtual_base::0x%lx||virtual_base_offset::0x%lx\n", filename, the_str, next_str, base_addr, str_based_offset);
        pos = (str_len + 1) + next_str;
        num_strs++;
        free (the_str);
    }

    return num_strs;
}

int main (int argc, char **argv) {
    char *filename = argc > 1 ? argv[1] : NULL;
    char *addr = argc > 2 ? argv[2] : NULL ;
    char *base_addr = argc > 3 ? argv[3] : NULL ;
    char *min_len_str = argc > 4 ? argv[4] : NULL ;
    uint32_t addr_len = filename && addr ? strlen (addr) : 0;
    uint32_t base_addr_len = filename && base_addr ? strlen (base_addr) : 0;
    uint32_t min_len_str_len = filename && min_len_str ? strlen (min_len_str) : 0;
    uint32_t min_len = 5;
    int64_t offset = 0, base_offset = 0;
    uint8_t *buf = NULL;
    FILE * f = filename && addr ? fopen (filename, "rb") : NULL;
    uint64_t sz = 0, read_sz = 0;
    char *name = NULL;
    uint32_t num_strs = 0;
    RBinJavaObj *rbj = NULL;

    if (!f || addr_len == 0 || !addr || !filename || !base_addr) {
        fprintf (stderr, "class_sz <filename> <offset> <base_offset> [min_len]\n");
        return -1;
    }

    if (addr[0] == '0' && (addr[1] == 'x' || addr[1] == 'X') && addr_len  > 2) {
        char * end = (addr+(int)addr_len);
        offset = strtoll (addr, &end, 16);
    } else {
        char * end = (addr+(int)addr_len);
        offset = strtoll (addr, &end, 10);
    }

    if (base_addr[0] == '0' && (base_addr[1] == 'x' || base_addr[1] == 'X') && base_addr_len  > 2) {
        char * end = (base_addr+(int)base_addr_len);
        base_offset = strtoll (base_addr, &end, 16);
    } else {
        char * end = (base_addr+(int)base_addr_len);
        base_offset = strtoll (base_addr, &end, 10);
    }

    if (min_len_str[0] == '0' && (min_len_str[1] == 'x' || min_len_str[1] == 'X') && min_len_str_len  > 2) {
        char * end = (min_len_str+(int)min_len_str_len);
        min_len = strtoll (min_len_str, &end, 16);
    } else {
        char * end = (min_len_str+(int)min_len_str_len);
        min_len = strtoll (min_len_str, &end, 10);
    }

    fseek (f, 0, SEEK_END);
    sz = ftell (f);
    buf = malloc (sz);
    fseek (f, 0, SEEK_SET);
    read_sz = fread (buf, 1, sz, f);
    fclose(f);

    num_strs = find_and_dump_cstrs (filename, buf, offset, base_offset,  sz, min_len);

    free (buf);
}
